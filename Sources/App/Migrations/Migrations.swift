//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//

import Fluent

let app_migrations: [Migration] = [
    TagMigration(),
    UserMigration(),
    TokenMigration(),
    UserVerificationTokenMigration(),
    DotMigration(),
    MemberMigration()
]

struct TagMigration { }
struct UserMigration { }
struct TokenMigration { }
struct UserVerificationTokenMigration { }

struct DotMigration {}
struct MemberMigration { }
