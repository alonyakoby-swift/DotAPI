//
//  MemberController.swift
//
//
//  Created by Alon Yakoby on 28.11.23.
//

import Vapor

final class MemberController: RouteCollection {
    let repository: StandardControllerRepository<Member>
    
    init(path: String) {
        self.repository = StandardControllerRepository<Member>(path: path)
    }
    
    func setupRoutes(on app: RoutesBuilder) throws {
        let route = app.grouped(PathComponent(stringLiteral: repository.path))
        route.post(use: repository.create)
        route.post("batch", use: repository.createBatch)

        route.get(use: repository.index)
        route.get(":id", use: repository.getbyID)
        route.delete(":id", use: repository.deleteID)
        
        route.patch(":id", use: repository.updateID)
        route.patch("batch", use: repository.updateBatch)
    }

    func boot(routes: RoutesBuilder) throws {
        try setupRoutes(on: routes)
    }
}

extension Member: Mergeable {
    func merge(from other: Member) -> Member {
        let merged = self
        merged.id = other.id
        merged.user = other.user
        merged.bio = other.bio
        merged.tel = other.tel
        merged.profilePicture = other.profilePicture
        merged.links = other.links
        return merged
    }
}
