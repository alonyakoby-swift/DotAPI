//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
import Vapor
import Fluent

enum DotType: String, Codable {
    case standard = "standard"
    case repost = "repost"
}

final class Dot: Model, Content, Codable {
    static let schema = "dots"

    @ID(custom: FieldKeys.id) var id: UUID?
    @Enum(key: FieldKeys.type) var type: DotType
    @Parent(key: FieldKeys.author) var author: Member
    @Field(key: FieldKeys.timestamp) var timestamp: Date
    @Field(key: FieldKeys.content) var content: String
    @Field(key: FieldKeys.repost) var repost: Int
    @Field(key: FieldKeys.likes) var likes: Int
    @Field(key: FieldKeys.views) var views: Int
    @OptionalField(key: FieldKeys.link) var link: String?
    @Field(key: FieldKeys.comments) var comments: [Comment]

    struct FieldKeys {
        static var id: FieldKey { "id" }
        static var type: FieldKey { "type" }
        static var author: FieldKey { "author" }
        static var timestamp: FieldKey { "timestamp" }
        static var content: FieldKey { "content" }
        static var repost: FieldKey { "repost" }
        static var likes: FieldKey { "likes" }
        static var views: FieldKey { "views" }
        static var link: FieldKey { "link" }
        static var comments: FieldKey { "comments" }
    }

    init() {}

    init(id: UUID? = nil, type: DotType, authorId: Member.IDValue, timestamp: Date, content: String, repost: Int, likes: Int, views: Int, link: String?, comments: [Comment]) {
        self.id = id
        self.type = type
        self.$author.id = authorId
        self.timestamp = timestamp
        self.content = content
        self.repost = repost
        self.likes = likes
        self.views = views
        self.link = link
        self.comments = comments
    }
}

extension DotMigration: Migration {
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        return database.schema(Dot.schema)
            .field(Dot.FieldKeys.id, .uuid, .identifier(auto: true))
            .field(Dot.FieldKeys.type, .string, .required)
            .field(Dot.FieldKeys.author, .uuid, .required, .references("members", "id"))
            .field(Dot.FieldKeys.timestamp, .datetime, .required)
            .field(Dot.FieldKeys.content, .string, .required)
            .field(Dot.FieldKeys.repost, .int, .required)
            .field(Dot.FieldKeys.likes, .int, .required)
            .field(Dot.FieldKeys.views, .int, .required)
            .field(Dot.FieldKeys.link, .string)
            .field(Dot.FieldKeys.comments, .array(of: .json), .required)
            .create()
    }

    func revert(on database: Database) -> EventLoopFuture<Void> {
        return database.schema(Dot.schema).delete()
    }
}
