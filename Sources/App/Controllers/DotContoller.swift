//
//  Dot Controller .swift
//  
//
//  Created by Alon Yakoby on 28.11.23.
//

import Vapor

final class DotController: RouteCollection {
    let repository: StandardControllerRepository<Dot>
    
    init(path: String) {
        self.repository = StandardControllerRepository<Dot>(path: path)
    }
    
    func setupRoutes(on app: RoutesBuilder) throws {
        let route = app.grouped(PathComponent(stringLiteral: repository.path))
        route.post(use: repository.create)
        route.post("batch", use: repository.createBatch)

        route.get(use: repository.index)
        route.get(":id", use: repository.getbyID)
        route.delete(":id", use: repository.deleteID)
        
        route.patch(":id", use: repository.updateID)
        route.patch("batch", use: repository.updateBatch)
        
        route.post(":id", "like", use: addLike)
        route.post(":id", "dislike", use: subtractLike)
        route.post(":id", "comment", use: addComment)
        route.post("views", use: incrementViews)
    }

    func boot(routes: RoutesBuilder) throws {
        try setupRoutes(on: routes)
    }
    
    func addLike(req: Request) throws -> EventLoopFuture<Dot> {
        Dot.find(req.parameters.get("id"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { dot in
                dot.likes += 1
                return dot.update(on: req.db).map { dot }
            }
    }

    func subtractLike(req: Request) throws -> EventLoopFuture<Dot> {
        Dot.find(req.parameters.get("id"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { dot in
                dot.likes = max(dot.likes - 1, 0)
                return dot.update(on: req.db).map { dot }
            }
    }

    func addComment(req: Request) throws -> EventLoopFuture<Dot> {
        let comment = try req.content.decode(Comment.self)
        return Dot.find(req.parameters.get("id"), on: req.db)
            .unwrap(or: Abort(.notFound))
            .flatMap { dot in
                dot.comments.append(comment)
                return dot.update(on: req.db).map { dot }
            }
    }

    func incrementViews(req: Request) throws -> EventLoopFuture<[Dot]> {
        let ids = try req.content.decode([UUID].self)
        return ids.map { id in
            Dot.find(id, on: req.db)
                .unwrap(or: Abort(.notFound))
                .flatMap { dot in
                    dot.views += 1
                    return dot.update(on: req.db).map { dot }
                }
        }.flatten(on: req.eventLoop)
    }
}

extension Dot: Mergeable {
    func merge(from other: Dot) -> Dot {
        let merged = self
        merged.type = other.type
        merged.author = other.author
        merged.timestamp = other.timestamp
        merged.content = other.content
        merged.repost = other.repost
        merged.likes = other.likes
        merged.views = other.views
        merged.link = other.link
        merged.comments = other.comments
        return merged
    }
}
// TODO: repost an exisiting dot
