//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation

struct Log: Codable {
    var id: UUID
    var message: String
    var type: String?
    var date: Date
    var member: Member
    
}
