//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//

import Vapor
import Fluent

struct UserSignup: Content {
    public let id: String
    public let email: String
    public let password: String
    
    init(id: String, email: String, password: String) {
        self.id = id
        self.email = email
        self.password = password
    }
}

struct NewSession: Content {
    let token: String
    let user: User.Public
}
