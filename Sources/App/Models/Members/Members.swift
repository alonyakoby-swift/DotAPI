//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//

import Vapor
import Fluent

struct SocialLink: Codable {
    var id: UUID?
    var url: String
    var type: SocialLinkType
}

enum SocialLinkType: String, Codable {
    case facebook
    case google
    case linkedIn
}

final class Member: Model, Content, Codable {
    static let schema = "members"

    @ID(custom: FieldKeys.id) var id: UUID?
    @Parent(key: FieldKeys.user) var user: User
    @Field(key: FieldKeys.bio) var bio: String
    @Field(key: FieldKeys.tel) var tel: String
    @Field(key: FieldKeys.profilePicture) var profilePicture: String
    @Field(key: FieldKeys.links) var links: [SocialLink]

    struct FieldKeys {
        static var id: FieldKey { "id" }
        static var user: FieldKey { "user_id" }
        static var bio: FieldKey { "bio" }
        static var tel: FieldKey { "tel" }
        static var profilePicture: FieldKey { "profile_picture" }
        static var links: FieldKey { "links" }
    }

    init() { }

    init(id: UUID? = nil, userId: UUID, bio: String, tel: String, profilePicture: String, links: [SocialLink]) {
        self.id = id
        self.$user.id = userId
        self.bio = bio
        self.tel = tel
        self.profilePicture = profilePicture
        self.links = links
        
    }
}

extension MemberMigration: Migration {
    func prepare(on database: Database) -> EventLoopFuture<Void> {
        return database.schema(Member.schema)
            .field(Member.FieldKeys.id, .uuid, .identifier(auto: true))
            .field(Member.FieldKeys.user, .uuid, .required, .references("users", "id"))
            .field(Member.FieldKeys.bio, .string, .required)
            .field(Member.FieldKeys.tel, .string, .required)
            .field(Member.FieldKeys.profilePicture, .string, .required)
            .create()
    }

    func revert(on database: Database) -> EventLoopFuture<Void> {
        return database.schema(Member.schema).delete()
    }
}
