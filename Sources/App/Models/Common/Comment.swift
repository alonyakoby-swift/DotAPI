//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation

struct Comment: Codable {
    var id: String
    var creator: String
    var content: String
    var images: [String]
    var timestamp: Date
}
